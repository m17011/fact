#include <stdio.h>
#include "fact.h"
#include "testCommon.h"

/*testFact.c by iiduka */

void testFact() {
    testStart("testFact");
}

int main() {
    testFact();
	assertEqualsInt(fact(0), 1);
	assertEqualsInt(fact(1),1);
	assertEqualsInt(fact(2), 2); // この行を追加
	assertEqualsInt(fact(3), 6); // この行を追加
	assertEqualsInt(fact(6), 720); // この行を追加
	assertEqualsInt(fact(-1), -1);
	assertEqualsInt(fact(-3), -1);
    testErrorCheck(); // この行は絶対に消さないこと
    return 0;
}
